// NOSTL

include <libopenscad/mtube.scad>;
include <libopenscad/mcylinder.scad>;
include <libopenscad/mcube.scad>;

wall_thickness = 2;

magnet_pocket_diameter = 3.2;
magnet_pocket_depth = 1.6;

bit_pocket_diameter = 7.5;
bit_socket_diameter = bit_pocket_diameter + wall_thickness * 2;

bit_pocket_depth =10;
bit_socket_depth = bit_pocket_depth + wall_thickness;


overlap = 0.02;


module post(oh = bit_pocket_depth + wall_thickness, od = bit_pocket_diameter + (wall_thickness * 2), id = bit_pocket_diameter, $fn = 180) {
    

     color("salmon") {
        // outside wall
        mtube(h = oh, od = od, id = id, chamfer = wall_thickness / 4, align = [0, 0, 1]);
    }

}


module socket(ih = bit_pocket_depth, od = bit_pocket_diameter + (wall_thickness * 2), id = bit_pocket_diameter, magnet_pocket = true, $fn = 180) {
    
    oh = ih + wall_thickness + magnet_pocket_depth;
    
    translate([0, 0, ((bit_pocket_depth - 10) / 2) + 0.25] ) {
    color("salmon") {
        // outside wall
        post(oh = oh, od = od, id = id);
    }


    difference() {
        color("blue") {
            // floor
            mcylinder(h = wall_thickness  + magnet_pocket_depth, d = od - overlap, chamfer = wall_thickness /4, center = false);
        }
        
        color("red") {
            // magnet pocket
            if(magnet_pocket) {
                translate([0, 0, wall_thickness]) {
                    mcylinder(h = magnet_pocket_depth + overlap, d = magnet_pocket_diameter);
                }
            }
        }
    }
}
}


//debug_socket = true;

if(!is_undef(debug_socket)) {
    socket(5);
}

//debug_post = true;

if(!is_undef(debug_post)) {
    rotate([0, -90, 0]) {
        post(oh = 90);
    }
}

