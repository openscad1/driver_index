// NOSTL

include <libopenscad/mtube.scad>;
include <libopenscad/mcylinder.scad>;
include <libopenscad/mcube.scad>;
include <libdriver2.scad>;

//total_width = 41;
total_width = 102;

bracket_thickness = 10;
axle_length = total_width - bracket_thickness;

$fn = 150;

gap = 0.1;

module axle() {
    rotate([0, 90, 0]) {
        color("lightblue") {
            post(axle_length);
        }
    }
}

module peg() {
    mcylinder(h = bracket_thickness, d = bit_pocket_diameter, chamfer = bit_pocket_diameter / 8);

}


module carrier() {
    translate([bracket_thickness/2, 0, 0]) {
        
        available_width = axle_length - bracket_thickness;
        echo("available_width", available_width);
        initial_socket_width = bit_socket_diameter;
        additional_socket_width = bit_socket_diameter - wall_thickness;
        assert(initial_socket_width < available_width);
        remaining_width = available_width - initial_socket_width;
        echo("remaining_width", remaining_width);
        echo(round(remaining_width / additional_socket_width));
        
        number_of_sockets = 1 + floor(remaining_width / additional_socket_width);
        echo("number_of_sockets", number_of_sockets);
        
        required_width = bit_socket_diameter + ((number_of_sockets - 1) * (bit_socket_diameter - wall_thickness));
        echo("required_width", required_width);
        
        difference() {

            axle();

union() {
                    translate([(bit_socket_diameter / 2) + ((axle_length - required_width) / 2), 0, -bit_socket_depth / 2]) {
            for(i = [0 : number_of_sockets - 1]) {
                translate([i * (bit_socket_diameter - wall_thickness), 0, 0]) {
                    socket();
                        color("pink") mcylinder(d = bit_socket_diameter - wall_thickness, h = bit_socket_depth * 3, center = true);
                }
            }
        }
    }
        }

        translate([(bit_socket_diameter / 2) + ((axle_length - required_width) / 2), 0, -bit_socket_depth / 2]) {
            for(i = [0 : number_of_sockets - 1]) {
                translate([i * (bit_socket_diameter - wall_thickness), 0, 0]) {
                    socket();
         
                }
            }
        }
        

    }

}


module hanger() {

    difference() {
        union() {
            end_cap();
            translate([0, bracket_thickness, -3]) {
                mcube([bracket_thickness, bracket_thickness / 2, 20], align = [1, 0, 0], chamfer = 1.25);
            }

            translate([axle_length + (bracket_thickness ), 0, 0]) {
                rotate([0, 0, 180]) {
                    end_cap();
                    translate([0, -bracket_thickness, -3]) {
                        mcube([bracket_thickness, bracket_thickness / 2, 20], align = [1, 0, 0], chamfer = 1.25);
                    }
                }
            }

            translate([0, 1.5, -10.5]) {
                // floor
                mcube([axle_length + (bracket_thickness), 21 + 3, 5], align = [1, 0, 0], chamfer = 1.25);
                translate([0, -6, -2.5]) {
                    mcube([axle_length + (bracket_thickness), 18, 7.25], align = [1, -1, 1], chamfer = 1.25);
                }
            }
            translate([0, 11, -3]) {
                // back wall
                mcube([axle_length + (bracket_thickness), 5, 20], align = [1, 0, 0], chamfer = 1.25);
            }

        }
        color("red") {
            translate([-bracket_thickness, 0, 0]) {
                rotate([0, 90, 0]) {
                    cylinder(d = bit_pocket_diameter + overlap + gap * 4, h = axle_length + bracket_thickness * 4);
                }
            }
        }
    }
}



module end_cap() {
    difference() {

        translate([0, 0, -3]) {
            mcube([10, 21, 20], align = [1, 0, 0], chamfer = 1.25);
        }

        translate([-gap, 0, -0.5]) {
            color("green") {
                hull() {
                    translate([5, 0, 0]) {
                        rotate([0, 90, 0]) {
                            mcylinder(d = bit_pocket_diameter + (wall_thickness * 2) + gap * 2 + 1, h = 5 + overlap + gap);
                        }
                        translate([0, 0, 20]) {
                            rotate([0, 90, 0]) {
                                mcylinder(d = bit_pocket_diameter + (wall_thickness * 2) + gap * 2 + 1, h = 5+ overlap + gap);
                            }
                        }
                    }
                }
            }
        }
    }
}