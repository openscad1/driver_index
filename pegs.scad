//NOSTL
include <libdriver.scad>;



// pegs
rotate([180, 0, 0]) {
    difference() {
        difference() { frame(); socket_rack(); }
        difference() { frame(open = true); socket_rack(); }
    }
}
