include <rack.scad>;

//color("fuchsia") mcube([10, 102, 10], align = [0, 1, 0], chamfer = 2.5);

translate([0, 14.5 + 8, 13]) {
    color("pink") hanger();

    translate([0, 33, 12.75 + 5.5]) {
        color("orange") hanger();

    translate([0, 33, 12.75 + 5.5]) {
            color("lightblue") hanger();
        }

    }
}


module wall() {
    translate([0, 102, 0]) {
        color("salmon") mcube([5, 69, 25], align = [1,-1,1], chamfer = 1.25);
        color("blue") mcube([5, 36, 43], align = [1,-1,1], chamfer = 1.25);

    }
    color("lightgreen") mcube([5, 17, 55.5], chamfer = 1.25);
    translate([0, 0, 17]) {
        color("yellow") {
            mcube([5, 50, 38.5], chamfer = 1.25);
        }
        translate([0, 0, 18.5]) {
            color("cyan") {
                mcube([5, 82, 21], chamfer = 1.25);
            }
        }
    }
}

wall();

translate([total_width -5, 0, 0]) {
    wall();
}
