//NOSTL


include <libopenscad/mtube.scad>;
include <libopenscad/mcylinder.scad>;
include <libopenscad/mcube.scad>;


magnet_pocket_diameter = 3.2;
magnet_pocket_depth = 1.6;

bit_pocket_diameter = 7.5;
bit_pocket_depth = 10;

mounting_post_fat_end = 10;

number_of_sockets = 7;


_debug_cutouts = true;
debug_rotate = true;

$fn = 100;

module slug() {
    union() {
        // hole for bit with magnet socket

        color("orange") {
            translate([0, 0, -magnet_pocket_depth / 2]) {
                cylinder(d = magnet_pocket_diameter, h = magnet_pocket_depth + 0.01, center = true);
            }
        }

        color("salmon") {
            translate([0, 0, bit_pocket_depth / 2]) {
                cylinder(d = bit_pocket_diameter, h = bit_pocket_depth + 0.02, center = true);
            }
        }

    }
}




module mounting_post() {
    translate([-(bit_pocket_diameter)/2, 0, 0]) {
        rotate([0, -90, 0]) {
            translate([0, 0, 0]) {
                color("lightblue") {
                    cylinder(h=10, d1=10, d2=10/2);
                }
            }
        }
    }
}

module bit_socket_v2(h = bit_pocket_depth) {
    difference() {
        union() {

            translate([0, 0, -5]){
                translate([0, 0, 5/2])
                        rotate([180, 0, 0])
                    difference() {
                        color("salmon") mcylinder(d = bit_pocket_diameter+ 5/2, h = 5/2);
                        color("red") translate([0, 0, -0.01]) cylinder(d = magnet_pocket_diameter, h = magnet_pocket_depth + 0.01);
                    }


                    mtube(od = bit_pocket_diameter+ 4, id = bit_pocket_diameter, h = h + magnet_pocket_depth + 5- (2.5 + magnet_pocket_depth) + (5/4)/2, chamfer = (5/4)/2, align =  [0,0,1]);
            }
        }
        
        if(!is_undef(debug_cutouts)) {
            color("red") translate([0, 0, -25]) cube([50, 50, 50]);
        }
    }
}

module socket(n = number_of_sockets, pins = false, h = bit_pocket_depth) {
    difference() {
        union() {
            // sockets
            translate([(100 - ((n - 1) * (bit_pocket_diameter + 2.5))) / 2, 0, 0]) {
                for(x = [0 : (n - 1)]) {
                    translate([x * (bit_pocket_diameter + 2.5), 0, 0]) {
                        bit_socket_v2(h);
                    }
                }
                if(pins) {
                    mounting_post();

                    translate([(n - 1) * (bit_pocket_diameter + 2.5), 0, 0]) {
                        rotate([0, 0, 180]) {
                            mounting_post();
                        }
                    }
                }
            }
        }
        if(!is_undef(debug_cutouts)) {
            translate([0, 0, -20]) color("red") cube([50, 20, 40]); 
        }
    }
}


module socket_rack(n = number_of_sockets, pins = true, h = bit_pocket_depth) {
    if(!is_undef(debug_rotate)) {
        rotate([abs(($t - 0.5) * 90 * 2) * -1, 0, 0]) {
            socket(n, pins, h);
        }
    } else {
        rotate([13, 0, 0]) {
            socket(n, pins, h);
        }
    }
}





module frame(open = false) {
    union() {

        union() {
            translate([0, 0, -15]) {
                // end caps
                translate([0, -10, 0]) {
                    difference() {
                        color("violet") {
                            mcube([13.25, 20 , 22.5], chamfer = 1);
                        }
                        translate([7, 5, 14]) {
                            color("red") {
                                if(open) {
                                    mcube([7, 10, 22.51 ], chamfer = 1);
                                }
                            }
                        }
                    }
                }
                translate([100 - 13.25, -10, 0]) {
                    difference() {
                        color("violet") {
                            mcube([13.25, 20 , 22.5], chamfer = 1);
                        }
                        translate([0, 5, 14]) {
                            color("red") {
                                if(open) {
                                    mcube([7, 10, 22.51 ], chamfer = 1);
                                }
                            }
                        }
                    }
                }
                
                

                difference() {
                    union() {
                    // measuring crossbar


                        color("pink") {
                            union() {
                                translate([0, 5, 0]) {
                                    mcube([100, bit_pocket_depth-2.5- (2.5 + magnet_pocket_depth), 10], chamfer = 1);
                                }
                                translate([0, -10, 0]) {
                                    mcube([100, 20, 5], chamfer = 1);
                                }

                            }
                        }
                    }
                    union() {
                        color("orange")
                        translate([(100 - ((number_of_sockets - 1) * (bit_pocket_diameter + 2.5))) / 2, 0, 15]) {
                            rotate([90, 0, 0])
                            for(x = [0 : (number_of_sockets - 1)]) {
                                translate([x * (bit_pocket_diameter + 2.5), 0, 0]) {
                                    mtube(od = bit_pocket_diameter+ 5, id = 0, h = 100, align =  [0,0,-0.5]);
                                }
                            }
                        }
                    }
                }

                //backrest
                color("lightblue") {
                    translate([0, -13, 0]) {
                        hull() {
                            mcube([100, 5, 22.5], chamfer = 1);
                            mcube([100, 6, 5], chamfer = 1);
                        }
                    }
                }
                
            }
        }


    }
    
}

rotate([90,0,0])
socket_rack(3, pins = false, h = 20);
