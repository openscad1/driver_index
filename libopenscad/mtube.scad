// BOF

// NOSTL

module mtube(h, od, id, align = [0, 0, 0], chamfer = 0) {

    translate([(od / 2) * align.x, (od / 2) * align.y, (h / 2) * (align.z - 1)]) {

        rotate_extrude() {
            polygon(points = [
            [id / 2          , chamfer],
            [id / 2 + chamfer, 0      ],
            [od / 2 - chamfer, 0      ],
            [od / 2          , chamfer],
            [od / 2          , h - chamfer],
            [od / 2 - chamfer, h],
            [id / 2 + chamfer, h],
            [id / 2          , h - chamfer]
            ]
            );
        }
    }
}







if(!is_undef(debug_lib)) {

    mtube(height = 40, od = 40, id = 30, chamfer = (40 - 30) / 2 / 4);
    mtube(height = 20, od = 60, id = 50, chamfer = 1);

}


echo("\n
\nmtube usage:
\n\tmodule mtube(h, od, id, align = [0, 0, 0], chamfer = 0)
\n
\n
");



// EOF
