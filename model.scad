include <rack.scad>;

// total_width = 100; bit_pocket_depth = 25;
// total_width = 100; bit_pocket_depth = 10;

r = 01 ? $t : .0;

difference() {
union() {
    // model
    color("orange") {
        difference() {
            hanger();
        }
    }


    
    rotate([(-11 + (abs(r - 0.5) * (90 + 22))) * 2, 0, 0]) {
        // rotate CCW 11 degrees
        // swing CW-then-CCW through (90 + 22) degrees
        carrier();
    }

    color("green") {
        rotate([0, 90, 0]) {
            peg();
        }

        translate([axle_length + bracket_thickness, 0, 0]) {
            rotate([0, -90, 0]) {
                peg();
            }
        }

    }     
}

    //mcube([40, 50, 50], align = [0, 0, 0]);
}